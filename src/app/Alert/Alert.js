import React, { Component } from 'react';

export default class Alert extends Component{
    constructor(props){
        super(props);
    }

    render() {
        return (this.props.visible === true ? (
            <div className="row">
                <div className="col-lg-12">
                    <div className={"alert alert-" + this.props.type }>
                        <strong>{this.props.title}</strong> {this.props.message}
                    </div>
                </div>
            </div>
        ): null);
    }
}

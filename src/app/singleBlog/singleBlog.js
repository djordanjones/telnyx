import React, { Component } from 'react';
import * as axios from 'axios';
import AddComment from '../AddComment/AddComment';

export default class singleBlog extends Component {
    constructor(props){
        super(props);

        this.state = {
          "id": null,
          "title": null,
          "author": null,
          "publish_date": null,
          "slug": null,
          "description": null,
          "content": null,
          "comments": [],
          "showCommentForm": false
        }
    }

    addCommentClick() {
      const state = this;

      state.setState({
        "showCommentForm": !this.state.showCommentForm
      })
    }

    fetchData() {
      const id = this.props.params.id;
      const state = this;

          axios.get('http://localhost:9001/posts/'+id)
            .then((res) => {
                state.setState({
                  "id": res.data.id,
                  "title": res.data.title,
                  "author": res.data.author,
                  "publish_date": res.data.publish_date,
                  "slug": res.data.slug,
                  "description": res.data.description,
                  "content": res.data.content
                })
            })
            .catch((e) => {
              console.log(e);
            })

            axios.get('http://localhost:9001/comments?postId='+id)
              .then((res) => {
                  state.setState({
                    "comments": res.data
                  })
              })
              .catch((e) => {
                console.log(e);
              })
    }

    componentDidMount() {
      this.fetchData();
    }

    render() {
        return (
          <div>
              <h1>{this.state.title}</h1><br />
              <h4>Author: {this.state.author}</h4><br />
              <div>Slug: {this.state.slug}</div>
              <div>Description: {this.state.description}</div><br />
              <div>{this.state.content}</div><br />
              <div>Date: {this.state.publish_date}</div>

              <br />
              <button onClick={() => {this.addCommentClick()}} type="button" className="btn btn-primary">Add Comment</button>
              <AddComment
                visible={this.state.showCommentForm}
                id={this.state.id}
              />
              <br />
              <br />
              <h3>Comments</h3>
              {this.state.comments.map((item) => {
                  return (
                        <div key={item.id}>
                          <span>Author: {item.user}</span><br />
                          <span>Comment: {item.content}</span><br />
                          <span>Date: {item.date}</span>
                          <hr />
                          <br />
                        </div>
                  )
              })}

            </div>
        )
    }
}

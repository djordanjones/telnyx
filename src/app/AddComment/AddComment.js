import React, {Component} from 'react';
import Alert from '../Alert/Alert';
import * as axios from 'axios';

export default class AddComment extends Component {
  constructor(props){
    super(props);

    this.state = {
      "name": null,
      "comment": null,
      "message": null,
      "visible": false
    }
  }

  handleCommentChange(event) {
    const state = this;

    state.setState({
      "comment": event.target.value
    })
  }

  handleNameChange(event) {
    const state = this;

    state.setState({
      "name": event.target.value
    })
  }

  commentSubmit(event) {
    event.preventDefault()
    const state = this;
    const name = this.state.name;
    const comment = this.state.comment;
    const postId = this.props.id;

    if (name === null) {
        state.setState({
            "message": "Name cannot be empty",
            "visible": true
        })
        return null;
    }

    if (comment === null) {
        state.setState({
            "message": "Comment cannot be empty",
            "visible": true
        })
        return null;
    }

    var params = new URLSearchParams();
    params.append('user', name);
    params.append('content', comment);
    params.append('postId', postId);
    params.append('date', '2017-12-12')

    axios.post('http://localhost:9001/comments', params)
    .then((response) => {
        this.state = {
            "message": `Comment ${response.data.id} created`,
            "visible": true
        }
    })
  }

  render() {
    return (
      this.props.visible === true ? (
        <div ClassName="row">
        <br />
        <br />
		<div ClassName="span12">

    <Alert
        message={this.state.message}
        visible={this.state.visible}
        type={'info'}
    />

			      <legend ClassName="">New Comment</legend>

            <label ClassName="control-label">Name</label>
			      <div ClassName="controls">
			        <input onChange={(event) => this.handleNameChange(event)} type="text" id="username" name="name" />
			      </div>

			      <label ClassName="control-label" htmlFor="password">Comment</label>
			      <div ClassName="controls">
            <textarea onChange={(event) => this.handleCommentChange(event)} rows="4" cols="50"> </textarea>
			      </div>
          <br />
			      <div ClassName="controls">
            <button className="btn btn-success" type="button"  onSubmit={() => {return false;}} onClick={(event) => {this.commentSubmit(event)}}>Submit</button>
			      </div>
		</div>
	</div>
      ): null);
  }
}

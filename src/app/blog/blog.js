import React, { Component } from 'react';
import * as db from "../../../api/db.json";
import { Link } from "react-router";
import * as axios from "axios";

export default class blog extends Component {
    constructor(props){
        super(props);

        this.state = {
            "posts" : [],
            "comments": []
        }
    }

    fetchData() {
      const id = this.props.params.id;
      const state = this;

          axios.get('http://localhost:9001/posts/')
            .then((res) => {
                state.setState({
                  "posts": res.data
                })
            })
            .catch((e) => {
              console.log(e);
            })
    }

    componentDidMount() {
      this.fetchData();
    }

    render() {
        return (
            <div className="container">
            {this.state.posts.map((item) => {
                return (
                    <Link to={`/blog/${item.id}`}>
                      <div
                        key={item.id}>

                        <span>{item.title}</span><br />
                        <span>Author: {item.author}</span><br />
                        <span>Date: {item.publish_date}</span>
                      </div>
                      <br />
                    </Link>
                )
            })}
            </div>
        )
    }
}

/**
 * Application entry point.
 */

// Global application styles
import "src/app.scss";

// React
import React from "react";
import {render} from "react-dom";
import {Router, Route, IndexRoute, Redirect, browserHistory} from "react-router";

// Our app
import App       from "./app/App";
import { About } from "./app/about/About";
import { Home }  from "./app/home/Home";

// Blog component Author: Jordan D Jones <jjones@dbtech.io>
import blog from "./app/blog/blog";
import singleBlog from "./app/singleBlog/singleBlog";

render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home}/>
      <Route path="about" component={About}/>
      <Route path="home" component={Home}/>


      {/* Author Jordan D. Jones <jjones@dbtech.io> */}
      <Route path="blog" component={blog}/>
      <Route path="blog/:id" component={singleBlog}/>
      {/* Author Jordan D. Jones <jjones@dbtech.io> */}

      <Redirect from="*" to="/home"/>
    </Route>
  </Router>
), document.getElementById("root"));
